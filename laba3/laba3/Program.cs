﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace laba3
{
    public class Program
    {
        static void Main(string[] args)
        {

            int[] sizes = { 1000, 10000, 100000, 1000000 };
            int report;
            do
            {
                Console.WriteLine("How array you want?:1.SortArray, 2.Reverse SortArray 3. RandomArray ");
                int change = int.Parse(Console.ReadLine());
                Console.WriteLine("How you want Sort?: 1.Bubble 2.Merge ");
                int typesort = int.Parse(Console.ReadLine());
                Console.WriteLine("Count elements  Count Operation ");

                for (int i = 0; i < sizes.Length; i++)
                {
                    int size = sizes[i];
                    int[] array = new int[size];
                    switch (change)
                    {
                        case 1: FillUp(array); break;
                        case 2: FillDown(array); break;
                        case 3: FillRandom(array); break;
                    }
                    long countOperation = 0;
                    switch (typesort)
                    {
                        case 1: countOperation = BubbleSort(array); break;
                        case 2: MergeSort(array, 0, array.Length - 1, ref countOperation); break;
                    }

                    Console.WriteLine(size + "  " + countOperation);

                }
                Console.WriteLine("1. Exit 2. Continue");
                report = int.Parse(Console.ReadLine());
            } while (report != 1);
        }
        public static void FillRandom(int[] a)
        {
            Random rand = new Random();
            for (int i = 0; i < a.Length; i++)
            {
                a[i] = rand.Next(100);
            }
        }
        public static void FillUp(int [] a)
        {
            for (int i = 0; i < a.Length; i++)
            {
                a[i] = i;
            }
        }
        public static void FillDown(int [] a)
        {
            for (int i = 0; i < a.Length; i++)
            {
                a[i] = a.Length - i;
            }
        }
        public static long BubbleSort(int [] a)
        {
            long countOperation = 0;
            for (int i = 0; i < a.Length-1; i++)
            {
                bool change = false;
                
                for (int j = 0; j < a.Length-1-i; j++)
                {
                    countOperation++;
                    if (a[j] > a[j+1])
                    {
                        countOperation++;
                        int t= a[j];
                        a[j] = a[j+1];
                        a[j + 1] = t;
                        change = true;
                    }
                }
                if (change == false) break;
            }
            return countOperation;

        }
        public static void Pechat(int [] a)
        {
            for (int i = 0; i < a.Length; i++)
            {
                Console.Write(a[i] +" ");
            }
            Console.WriteLine();
        }
        public  static void Merge(int[] Mas, int left, int right, int medium, ref long countOperation)
        {
            int j = left;
            int k = medium + 1;
            int count = right - left + 1;

            if (count <= 1) return;

            int [] TmpMas = new int[count];

            for (int i = 0; i < count; ++i)
            {
                if (j <= medium && k <= right)
                {
                    countOperation += 2;
                    if (Mas[j] < Mas[k])  
                        TmpMas[i] = Mas[j++];
                    else
                        TmpMas[i] = Mas[k++];
                }
                else
                {
                    countOperation += 2;
                    if (j <= medium)
                    
                        TmpMas[i] = Mas[j++];
                    else
                        TmpMas[i] = Mas[k++];
                }
            }

            j = 0;
            for (int i = left; i <= right; ++i)
            {
                Mas[i] = TmpMas[j++];
            }
           
        }
        public static void MergeSort(int [] a, int left, int right, ref long countOperation)
        {
            int middle;

            // Условие выхода из рекурсии
            if (left >= right) return;

            middle = left + (right - left) / 2;

            // Рекурсивная сортировка полученных массивов
            MergeSort(a, left, middle, ref countOperation);
            MergeSort(a, middle + 1, right, ref countOperation);
            Merge(a, left, right, middle, ref countOperation);
        }
    }
}
