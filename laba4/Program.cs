﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab4
{
    public class Program
    {
		public static int LevenshteinDistance(string first, string second)
		{
			int[,] opt = new int[first.Length + 1, second.Length + 1];
			for (var i = 0; i <= first.Length; ++i) opt[i, 0] = i;
			for (var i = 0; i <= second.Length; ++i) opt[0, i] = i;
			for (var i = 1; i <= first.Length; ++i)
				for (var j = 1; j <= second.Length; ++j)
				{
					if (first[i - 1] == second[j - 1])
						opt[i, j] = opt[i - 1, j - 1];
					else
						opt[i, j] = 1 + Math.Min(Math.Min(opt[i - 1, j], opt[i, j - 1]), opt[i - 1, j - 1]);
				}
			return opt[first.Length, second.Length];
		}

		static void Main(string[] args)
        {
            string[] words = {"кот ","солдат ","солнце ","море ","оценка ","предмет ","секрет ","код ","крот ","мод"};
            Console.WriteLine("Набор слов для автокоррекции таков: "+string.Join(" ",words));
            while (true)
            {
                Console.Write("Введите слово ");
                string w = Console.ReadLine();
                string findw = "";
                int mindistance = 100;
                for (int i = 0; i < words.Length; i++)
                {
                    int distance = LevenshteinDistance(w, words[i]);
                    if (distance <= mindistance)
                    {
                        mindistance = distance;
                        findw = words[i];
                    }
                }
                Console.WriteLine("Наиболе подхоящая замена: "+findw);
                
            }

        }
    }
}
