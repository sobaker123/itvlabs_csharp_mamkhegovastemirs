﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace laba2._2
{
    class Program
    {
        /*•	Составьте линейную программу, печатающую значение 1, если указанное высказывание является истинным, и 0 – в противном случае. 
         * Величина d является корнем только одного из уравнений ax2 + bx + c = 0 и mx + n = 0 относительно х.*/
        static void Main(string[] args)
        {
            int a, b, c, m, n;
            double x1 = 0, x2 = 0, x3;
            Console.Write("Enter a: ");
            a = int.Parse(Console.ReadLine());
            Console.Write("Enter b: ");
            b = int.Parse(Console.ReadLine());
            Console.Write("Enter c: ");
            c = int.Parse(Console.ReadLine());

            int d = b * b - 4 * a * c;
            if (d >= 0)
            {
                x1 = (-b + Math.Sqrt(d)) / (2 * a);
                x2 = (-b - Math.Sqrt(d)) / (2 * a);
            }
            Console.Write("Enter m: ");
            m = int.Parse(Console.ReadLine());
            Console.Write("Enter n: ");
            n = int.Parse(Console.ReadLine());

            x3 = (double)-n / m;
            Console.Write("Enter d: ");
            double dd = double.Parse(Console.ReadLine());

            bool res = x1 == dd && x3 != dd || x2 == dd && x3 != dd || x3 == dd && x1 != dd || x3 == dd && x2 != dd;
            if (res == true) Console.WriteLine(1); else Console.WriteLine(0);
            Console.ReadLine();
                }
    }
}
